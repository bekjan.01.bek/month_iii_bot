import os
import random

from aiogram import Bot, Dispatcher, types
from aiogram.utils import executor
from decouple import config


TOKEN = config("TOKEN")

bot = Bot(TOKEN)
dp = Dispatcher(bot=bot)


@dp.message_handler(commands=['start'])
async def start_command(message: types.Message):
    await message.answer(f"Привет радной родный газ природный как ты {message.from_user.full_name} ")


@dp.message_handler(commands=['myinfo'])
async def myinfo_command(message: types.Message):
    user_id = message.from_user.id
    username = message.from_user.username
    name = message.from_user.first_name
    info = f'Ваш id = {user_id}\n' \
           f'Ваш никнейм : {username}\n' \
           f'Ваше имя : {name}'
    await message.reply(info)


@dp.message_handler(commands=['picture'])
async def send_picture(message: types.Message):
    photos_path = "images"
    photo_files = os.listdir(photos_path)
    random_photo = random.choice(photo_files)
    photos_path = os.path.join(photos_path, random_photo)
    with open(photos_path, 'rb') as photo_file:
        await bot.send_photo(chat_id=message.chat.id, photo=photo_file.read())


@dp.message_handler()
async def echo(message: types.Message):
    if message.text == 'Bailey':
        await message.answer('https://oc.kg/#/movie/id/12683')
    else:
        await bot.send_message(
            chat_id=message.from_user.id,
            text=f"Салам Алейкум как ты {message.from_user.full_name}")

        await message.answer(f"This is a an answer method! {message.message_id}")
        await message.reply("This is a an reply method!")



if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)



